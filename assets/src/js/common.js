(function (window, document) {
  "use strict";
  const retrieveURL = function (filename) {
    let scripts = document.getElementsByTagName('script');
    if (scripts && scripts.length > 0) {
      for (let i in scripts) {
        if (scripts[i].src && scripts[i].src.match(new RegExp(filename + '\\.js$'))) {
          return scripts[i].src.replace(new RegExp('(.*)' + filename + '\\.js$'), '$1');
        }
      }
    }
  };


  function load(url, element) {
    let req = new XMLHttpRequest();

    req.onload = function () {
      if (this.readyState == 4 && this.status == 200) {
        element.insertAdjacentHTML('afterbegin', req.responseText);
      }
    };

    req.open("GET", url, true);
    req.send(null);
  }

  if (
    location.hostname !== "localhost" &&
    location.hostname !== "127.0.0.1" &&
    location.host !== ""
  ) {

    //var files = ["/assets/build/img/symbol_sprite.html", "/assets/src/img/svg/*"],
    //   var files = ["//mydance.beget.tech/verstka/img/symbol_sprite.html"],
    var files = ["https://dasglobal.ru/sklad0/img/symbol_sprite.html"],
      revision = 9;

    if (
      !document.createElementNS ||
      !document.createElementNS("http://www.w3.org/2000/svg", "svg")
        .createSVGRect
    )
      return true;

    var isLocalStorage =
      "localStorage" in window && window["localStorage"] !== null,
      request,
      data,
      insertIT = function () {
        document.body.insertAdjacentHTML("afterbegin", data);
      },
      insert = function () {
        if (document.body) insertIT();
        else document.addEventListener("DOMContentLoaded", insertIT);
      };

    files.forEach(file => {
      // console.log('file', file)
      // var re = /img\/symbol_sprite.html/gi
      // var newstr = file.replace(re, '')
      // console.log(newstr)
      // var a = document.querySelectorAll('.header-top-right-in li a'), i = a.length
      // a.forEach(el => {
      //   console.log('a.href', el.href)
      //   var reW = /test/gi
      //   let hrefLink = el.href
      //   var newhref = hrefLink.replace(reW, newstr)
      //   hrefLink = newhref
      // })
      try {
        let request = new XMLHttpRequest();

        request.open("GET", file, true);

        request.onload = function () {
          if (request.status >= 200 && request.status < 400) {
            data = request.responseText;
            insert();
            if (isLocalStorage) {
              localStorage.setItem("inlineSVGdata", data);
              localStorage.setItem("inlineSVGrev", revision);
              console.log('insert forEach isLocalStorage')
            }
          }
        };
        request.send();
      } catch (e) { }
    })
  }
  else {
    load("/img/symbol_sprite.html", document.querySelector("body"));
  }

  const btnModals = document.querySelectorAll('[data-toggle="modal"]').length !== 0 ? document.querySelectorAll('[data-toggle="modal"]') : false;
  if (btnModals) {
    btnModals.forEach((e) => {
      e.addEventListener("click", () => {
        let data_target = e.getAttribute("data-target")
        let src = e.getAttribute("data-src")
        let modal = document.querySelector(data_target)
        let iframe = jQuery(modal).find('iframe')
        iframe.length > 0 ? iframe.attr("src", src + '&autoplay=1') : false;

      })
    })
  }
  jQuery(document).on("click", "#modalVideoGallery", (e)=>{
    let iframe = jQuery('#modalVideoGallery').find('iframe')
    iframe.attr("src",'')
  })

})(window, document);
document.addEventListener("DOMContentLoaded", () => {
  const body = document.querySelector('body')
  const burgerNav = (burger, canvas, modal) => {
    console.log('modal', modal)
    let touchLength = 0
    const toggle = () => {
      burger.classList.toggle('active')
      canvas.classList.toggle('block')
      modal.classList.toggle('active')
      body.classList.toggle('active')
    }
    burger.onclick = () => {
      toggle()
    }
    modal.ontouchstart = (e) => {
      touchLength = e.touches[0].clientX
    }
    modal.ontouchend = (e) => {
      if (touchLength > (e.changedTouches[0].clientX + 60)) {
        toggle()
      }
    }
    modal.onclick = (e) => {
      if (e.target.tagName === 'A') toggle()
    }
    window.addEventListener('click', (e) => {
      if (e.target === canvas) toggle()
    })
  }
  console.log('window.screen.width', window.screen.width)
  if (window.screen.width <= 767){
    burgerNav(document.querySelector('.burger'), document.querySelector('.canvas'), document.getElementById('menu'))
  }

})

jQuery(function($){
  /* mask */
  $(".phone").mask("+7(999) 999-99-99");
  $('.benefits-gallery .benefits-gallery-video').mouseenter(e=>{
    $('.benefits-gallery').addClass('hover')
  })
  $('.benefits-gallery .benefits-gallery-video').mouseleave(e=>{
    $('.benefits-gallery').removeClass('hover')
  })
  var footerHeight = $('.footer').height(),
    headerHeight = $('.header').height(),
    pagUserHeight = $('.page-user').height(),
    sidebarFix = pagUserHeight - (footerHeight + headerHeight ),
    width = document.documentElement.clientWidth
  $(window).bind('scroll', function () {
    if ($(window).scrollTop() > 1) {
      //$('#scroll').fadeIn();
      $("body").addClass('scroll');
      $(".page-main").addClass('scroll');
      $(".header").addClass('fixed');
    } else {
      //$('#scroll').fadeOut();
      $("body").removeClass('scroll');
      $(".page-main").removeClass('scroll');
      $(".header").removeClass('fixed');
    }
    if (document.querySelector('.page-user')){
      console.log('width', width)
      if (width > 767){
        console.log(width)
        if ($(window).scrollTop() > sidebarFix){
          $(".page-user").removeClass('sidebarFix');
        } else {
          $(".page-user").addClass('sidebarFix');
        }

        if ($(window).scrollTop() == 0) $(".page-user").removeClass('sidebarFix')
      }
    }

  });
  $('.profitable_solution-slider').slick({
    centerMode: true,
    slidesToShow: 5,
    infinite: true,
    arrows: true,
    responsive: [
      {
        breakpoint: 1800,
        settings: {
          centerMode: true,
          slidesToShow: 4,
        }
      },
      {
        breakpoint: 1400,
        settings: {
          centerMode: true,
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 768,
        settings: {
          centerMode: true,
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: false,
          slidesToShow: 1.1
        }
      }
    ]
  });
  $('.section_das-slider').slick({
    centerMode: true,
    slidesToShow: 5,
    infinite: true,
    arrows: true,
    responsive: [
      {
        breakpoint: 1800,
        settings: {
          centerMode: true,
          slidesToShow: 4,
        }
      },
      {
        breakpoint: 1400,
        settings: {
          centerMode: true,
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 768,
        settings: {
          centerMode: true,
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: false,
          slidesToShow: 1
        }
      }
    ]
  });
  $('.extended_capabilities-slider').slick({
    slidesToShow: 3,
    infinite: false,
    arrows: true,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          arrows: false,
          slidesToShow: 1
        }
      }
    ]
  });
})







